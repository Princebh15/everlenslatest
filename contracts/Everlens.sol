pragma solidity ^0.5.0;

import "@openzeppelin/contracts/token/ERC721/ERC721Full.sol";
import "@openzeppelin/contracts/drafts/Counters.sol";


contract Everlens is ERC721Full {
    string public name;
    uint256 public productCount = 0;

    mapping(uint256 => Product) public products;

    struct Product {
        uint256 id;
        string name;
        string image;
        address payable owner;
        bool purchased;
        string creator;
        string holder;
    }
    event ProductApproved(uint256 _productId);

    event TransferProduct(uint256 _productId);

    event ProductCreated(
        uint256 id,
        string _name,
        string _image,
        address payable owner,
        bool purchased,
        string creator,
        string holder
    );

    event ProductUpdated(
        uint256 id,
        address from,
        address owner,
        bool purchased
    );

    constructor() public ERC721Full("Everlens", "EverlensCollection") {}

    function createProduct(
        address owner,
        string memory metadataURI,
        string memory _name,
        string memory _image,
        string memory _creator,
        string memory _holder
    ) public {
        require(bytes(_name).length > 0);
        // // Required Valid Price
        // require(_price > 0);
        //Increment product count
        productCount++;
        _mint(msg.sender, productCount);
        addItemMetadata(productCount, metadataURI);
        //create a product
        products[productCount] = Product(
            productCount,
            _name,
            _image,
            msg.sender,
            false,
            _creator,
            _holder
        );
        //Trigger the enter
        emit ProductCreated(productCount, _name, _image, msg.sender, false, _creator, _holder);
    }

    function getAddress() public view returns (address) {
        address myaddress = address(this); //contract address
        return myaddress;
    }

    function transfer(
        address _from,
        address payable _to,
        uint256 _token    ) public {
        safeTransferFrom(_from, _to, _token);
        Product memory _product = products[_token];
        // Fetch the Owner
        require(_product.id > 0 && _product.id <= productCount);
        // Check the product not be purchased before
        require(!_product.purchased);
        // check the seller can't buy his own product
        // Transfer the ownership to buyer
        _product.owner = _to;
        // Mark as Purchase
        _product.purchased = true;

        // Update the product
        products[_token] = _product;
        emit ProductUpdated(_token, _from, _product.owner, _product.purchased);
    }

    function addItemMetadata(uint256 _tokenId, string memory _uri)
        public
        returns (bool)
    {
        _setTokenURI(_tokenId, _uri);
        return true;
    }
}
