import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor(private router : Router) { }

  ngOnInit(): void {
  }

  // href='../../../assets/terms/PrivacyPolicy.html' target='#'
  // href='../../../assets/terms/terms.html' target='#'s

  privacyPolicy() {
    this.router.navigate(['/privacy-policy']);
  }

  termsAndConditions() {
    this.router.navigate(['/terms-conditions']);
  }

  saveit() {
    // let url="https://everlens.io/terms-conditions"
    let url="/terms-conditions"

    window.open(url, '_blank');
  }

  aboutPage() {
    let url="http://about.everlens.io/cgi-sys/defaultwebpage.cgi"
    window.open(url, '_blank');
  }

  privacy() {
    let url="https://everlens.io/privacy-policy"
    window.open(url, '_blank');
  }



}
