import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map } from 'rxjs/operators';
import { Web3Service } from 'src/app/service/web3.service';



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  keyword : string = '';
  @ViewChild('usersSearchInput', {static : false }) private usersSearchInput  : ElementRef
  filteredData : any = [];
  filterAllData : any = [];
  
 constructor(private toggleEle:ElementRef, private service : Web3Service) { 
  let userData = JSON.parse(localStorage.getItem('user-data'));

  this.service.clearSearch.subscribe(res => {
    if(res) { 
      this.keyword = "";   
    }
  })
 }


  ngOnInit(): void {
  }

  toggle(){
    let toggleAction = this.toggleEle.nativeElement.querySelector('#mySidenav');
    toggleAction.classList.add('opennav');
  }
  close(){
    let toggleAction = this.toggleEle.nativeElement.querySelector('#mySidenav');
    toggleAction.classList.remove('opennav');
  }

  ngAfterViewInit() {
   fromEvent(this.usersSearchInput.nativeElement, 'keyup').pipe(
     map((event : any) => {
       return event.target.value;
     }),
     filter(res => res.length>= 0),
     debounceTime(500),
     distinctUntilChanged()
   ).subscribe((text : string) => {
    this.service.searchData.next(this.keyword);
   })
 }
}
