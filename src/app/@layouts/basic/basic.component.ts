import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { WrongNetworkComponent } from 'src/app/@pages/wrong-network/wrong-network.component';
import { Web3Service } from 'src/app/service/web3.service';

@Component({
  selector: 'app-basic',
  templateUrl: './basic.component.html',
  styleUrls: ['./basic.component.scss']
})
export class BasicComponent implements OnInit {

  bsModalRef: BsModalRef;
  showDD: boolean = true;
  networkId : any;

  constructor(private modalService: BsModalService, private service : Web3Service) { 
    // this.service.checkNetwokId.subscribe((res:any)=>{
    //   if(res){
    //     this.getId();

    //   }
    // })
    // this.getId();

  }

  ngOnInit(): void {
    this.getId();

  }

  
  getId()  {
    this.service.getNetworkId().then((id: any) => {
        if (id) {
        this.networkId = id
        this.networkmodal();
      }
    });
  }

  networkmodal() {
    const initialState = {
      list: [
        'Open a modal with component',
        'Pass your data',
        'Do something else',
        '...'
      ],
      title: 'Modal with component'
    };
    if(this.networkId!=4)   {
      this.bsModalRef = this.modalService.show(WrongNetworkComponent,  {
        ignoreBackdropClick: true,
        keyboard: false
      });
      this.hideShowDrop(true);
    }else{
      this.service.isWalletConnected.next(true);
    }

    // this.bsModalRef.content.closeBtnName = 'Close';
  }
  
  hideShowDrop(isShow) {
    if(this.networkId==4)
      this.showDD = isShow;
  }
}