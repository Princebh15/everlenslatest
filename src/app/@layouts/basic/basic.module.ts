import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { FooterComponent } from 'src/app/@common/footer/footer.component';
import { HeaderComponent } from 'src/app/@common/header/header.component';
import { SwiperModule } from 'swiper/angular';
import { BasicRoutingModule } from './basic-routing.module';
import { BasicComponent } from './basic.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [BasicComponent, HeaderComponent, FooterComponent],
  imports: [
    CommonModule,
    BasicRoutingModule,  
    FormsModule,
    SwiperModule,
    BsDropdownModule.forRoot(),
  ]
})
export class BasicModule { }
