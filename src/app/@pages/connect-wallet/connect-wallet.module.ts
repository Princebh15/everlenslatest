import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConnectWalletRoutingModule } from './connect-wallet-routing.module';
import { ConnectWalletComponent } from './connect-wallet.component';


@NgModule({
  declarations: [ConnectWalletComponent],
  imports: [
    CommonModule,
    ConnectWalletRoutingModule
  ]
})
export class ConnectWalletModule { }
