import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
import { ActivatedRoute } from '@angular/router';
import { Web3Service } from 'src/app/service/web3.service';
import { IPFS } from 'src/app/ipfs/ipfs';
import { Buffer } from 'buffer';
import { Router } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-detail-page',
  templateUrl: './detail-page.component.html',
  styleUrls: ['./detail-page.component.scss']
})
export class DetailPageComponent implements OnInit {

  show: boolean;
  thumbs: any;
  private auction: any;
  private everlens: any;
  totalProduct = [];
  totalAuction = [];
  accountNumber: any;
  balance: any;
  itemMetadata: any;
  pagination: any = false;
  bidPrice: any;
  finaliseAuction: boolean = false;
  finaliseAuctionCompleted: boolean = false;
  transferAuction: boolean = false;
  transferAuctionCompleted: boolean = false;
  // malihu scrollbar options
  mediaUrl = 'https://everlens.io:3000/api/image?filename=';
  apiUrl = 'https://everlens.io:3000/api';
  public scrollbarOptions = { axis: 'y', theme: 'dark-thin' };
  @ViewChild('closeBtn') closeBtn: ElementRef;

  auction_id: any;
  itemData: any;
  bid: any;
  bidAmount: unknown;
  bidFrom: any;
  startPrice: unknown;
  bidCount: any;
  bids: any;
  owner: any;
  AuctionOwner: boolean;
  creator: any;
  userName: any;
  submitted: boolean = false;
  isLoading: boolean = false;
  isWalletConnect: boolean = false;
  profileImage:any='';
  highestBidderImg:string='';
  constructor(private activatedRoute: ActivatedRoute, @Inject(IPFS) private ipfs,
    private mScrollbarService: MalihuScrollbarService, private web3: Web3Service, private router: Router, private toastrService: ToastrService,private http:HttpClient) {

    this.activatedRoute.params.subscribe(param => {
      if (param['id']) {
        this.auction_id = param['id'];
      }
    });

    this.web3.checkAndInstantiateWeb3()
      .then((checkConn: any) => {

        if (checkConn === 'connected') {
          this.web3.loadBlockChainData()
            .then((accountData: any) => {
              this.accountNumber = accountData[0];
              this.web3.getEtherBalance(this.accountNumber)
                .then((data: any) => {
                  this.balance = Number(data).toFixed(2);
                });
              this.web3.getContract()
                .then((contractRes: any) => {
                  if (contractRes) {
                    this.everlens = contractRes;
                  }
                });

              this.web3.getAuctionContract()
                .then((contractRes: any) => {
                  if (contractRes) {
                    this.auction = contractRes;

                    ///method call to get detail;
                    // this.getItemDetail(this.auction_id)
                    this.getAuctionDetail();
                  }
                });
            })
        }
      }, err => {

      });
    this.userName = JSON.parse(localStorage.getItem('username'));

  }

  ngOnInit(): void {
    window.scrollTo({
      top: 0,
      left: 0
    });
    this.accountNumber = this.web3.walletAddress;
    // this.getcontract();
    // if(this.web3.isWalletConnected.value){
    //   this.getcontract();
    // }
    // this.web3.isWalletConnected.subscribe((res:boolean)=>{
    //   if(res){
    //     this.accountNumber =this.web3.walletAddress;
    //   }
    // });
  }
  getcontract() {
    this.web3.getAuctionContract()
      .then((contractRes: any) => {
        if (contractRes) {
          this.auction = contractRes;

          ///method call to get detail;
          // this.getItemDetail(this.auction_id)
          this.getAuctionDetail();
        }
      });
  }
  async placeBid(bidding?) {
    // if (this.isWalletConnect) {
      // this.accountNumber =this.web3
      this.submitted = true;
      this.isLoading = true;

      if (!bidding.invalid) {
        var ether = await this.web3.getValueToWei(this.bidPrice);
        var webOwned = this.bidPrice * 0.005;
        var webether = await this.web3.getValueToWei(webOwned.toString());

        var OwnerAmount = this.bidPrice - webOwned;
        var Ownedether = await this.web3.getValueToWei(OwnerAmount.toString());
        await this.auction.methods.bidOnAuction(this.auction_id, Ownedether, webether).send({ from: this.accountNumber, value: ether }).then(async (val) => {
          this.isLoading = false;
        });
      }
      this.isLoading = false;
      this.getAuctionDetail();
    // } else {
      // this.web3.connectWeb3.next(true);
    // }

  }

  finalizeAuction() {
    this.finaliseAuction = true
    var endDate = new Date().getTime() / 1000;
    this.auction.methods.finalizeAuction(this.auction_id, endDate.toFixed(0)).send({ from: this.accountNumber }).then(async (data) => {
      this.finaliseAuction = false;
      this.finaliseAuctionCompleted = true;
      this.transferAuction = true;
      var fromAddress = data.events.AuctionFinalized.returnValues.from;
      var token = data.events.AuctionFinalized.returnValues.productId;
      await this.everlens.methods.transfer(this.accountNumber, fromAddress, token).send({ from: this.accountNumber }).then(async (data) => {

        this.transferAuction = true;
        this.transferAuctionCompleted = true;
        setTimeout(() => {
          this.closeModal();
          this.router.navigate(['/home']);
        }, 2000);
      });
    })
  }

  closeModal(): void {
    this.closeBtn.nativeElement.click();
  }


  copyURL() {
    let msg = window.location.href;
    const urlBox = document.createElement('textarea');
    urlBox.style.position = 'fixed';
    urlBox.style.left = '0';
    urlBox.style.top = '0';
    urlBox.style.opacity = '0';
    urlBox.value = msg;
    document.body.appendChild(urlBox);
    urlBox.focus();
    urlBox.select();
    document.execCommand('copy');
    document.body.removeChild(urlBox);
    this.toastrService.success('Copied to clipboard.', '');
  }

  getAuctionDetail() {
    let auction_data = this.auction.methods.getAuctionById(this.auction_id).call({ from: this.accountNumber }).then(async (data) => {
      this.itemData = data;
      this.profileImage = await this.getImage(this.itemData.owner);
      this.startPrice = await this.web3.getValueFromWei(data.startPrice);
      var uri = data.metadata.slice('https://ipfs.io/ipfs/'.length)
      const source = await this.ipfs.cat(uri)
      let contents = ''
      const decoder = new TextDecoder('utf-8')

      for await (const chunk of source) {
        contents += decoder.decode(chunk, {
          stream: true
        })
      }

      if (this.accountNumber == data.owner) {
        this.AuctionOwner = true;

      } else {
        this.AuctionOwner = false
      }

      contents += decoder.decode()

      this.itemMetadata = JSON.parse(contents)

      await this.everlens.methods.products(data.productId)
        .call()
        .then(product => {
          this.creator = product.creator;
          this.owner = product.owner;
        });
      await this.auction.methods.getCurrentBid(this.auction_id).call({ from: this.accountNumber }).then(async (data) => {
        var ether = await this.web3.getValueFromWei(data[0]);
        this.bidAmount = ether;
        this.bidFrom = data[1];
        this.highestBidderImg= await this.getImage(this.bidFrom);

      });



      await this.auction.methods.getBidOfAuctions(this.auction_id).call({ from: this.accountNumber }).then(async (data) => {
        this.bids = [];
        data.map(async (x) => {
          var ether = await this.web3.getValueFromWei(x[1]);
          let img = await this.getImage(x[0]);
          this.bids.push({
            amount: ether,
            from: x[0],
            img:img
          });
        });
      });

      await this.auction.methods.getBidsCount(this.auction_id).call({ from: this.accountNumber }).then(async (data) => {
        this.bidCount = data;
      });
    });
  }

  async getImage(address: string) {
    return await this.http.get(this.apiUrl + '/profile?address=' + address).toPromise().then(res => {
      return res[0] ? this.mediaUrl + res[0].image : '';
    });
  }

}
