import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailPageRoutingModule } from './detail-page-routing.module';
import { DetailPageComponent } from './detail-page.component';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { MalihuScrollbarModule } from 'ngx-malihu-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [DetailPageComponent],
  imports: [
    CommonModule,
    DetailPageRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TabsModule.forRoot(),
    MalihuScrollbarModule.forRoot(),

  ]
})
export class DetailPageModule { }
