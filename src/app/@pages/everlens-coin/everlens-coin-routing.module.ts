import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EverlensCoinComponent } from './everlens-coin.component';

const routes: Routes = [
  {
    path:'',
    component:EverlensCoinComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EverlensCoinRoutingModule { }
