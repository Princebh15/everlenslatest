import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EverlensCoinRoutingModule } from './everlens-coin-routing.module';
import { EverlensCoinComponent } from './everlens-coin.component';


@NgModule({
  declarations: [
    EverlensCoinComponent
  ],
  imports: [
    CommonModule,
    EverlensCoinRoutingModule
  ]
})
export class EverlensCoinModule { }
