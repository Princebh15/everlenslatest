import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.scss']
})
export class MyAccountComponent implements OnInit {

  constructor(private toastrService: ToastrService) { 
    
  }

  ngOnInit(): void {
  }

  logout(){
    localStorage.clear();
    this.toastrService.success('Logout Successfully', '');

  }

}
