import { Component, OnInit, ViewChild, ChangeDetectorRef, Inject } from '@angular/core';
import { Web3Service } from 'src/app/service/web3.service';
import { IPFS } from 'src/app/ipfs/ipfs'
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { WrongNetworkComponent } from "../wrong-network/wrong-network.component"
import { NoMetaMaskComponent } from '../no-meta-mask/no-meta-mask.component';

@Component({
  selector: 'app-my-item',
  templateUrl: './my-item.component.html',
  styleUrls: ['./my-item.component.scss'],

})
export class MyItemComponent implements OnInit {
  show: boolean;
  thumbs: any;
  private auction: any;
  accountNumber: any;
  showDD: boolean = true;
  auctionList: any;
  balance: string;
  ownerAuctions = [];
  highestBidAmount: unknown;
  highestBidFrom: any;
  everlens: any;
  totalItems: any;
  auctionItems = [];
  boughtItems = [];
  itemMetadata: any;
  drop: number;
  activeAuctions = [];
  soldAuctions = [];
  bidAuctions = [];
  bsModalRef: BsModalRef;
  typeValue = 0;
  options = [{ "id": 0, "name": "All" }, { "id": 1, "name": "Active" }, { "id": 2, "name": "Sold" }];
  boughtItemss = [];
  auctionItemss = [];
  PurchaseItems = [];
  bidItems = [];
  constructor(private web3: Web3Service, private cd: ChangeDetectorRef, private modalService: BsModalService,
    @Inject(IPFS) private ipfs) {
    this.web3.checkAndInstantiateWeb3()
      .then((checkConn: any) => {

        if (checkConn === 'connected') {
          this.web3.loadBlockChainData()
            .then(async (accountData: any) => {
              this.accountNumber = accountData[0];
              this.web3.getEtherBalance(this.accountNumber)
                .then((data: any) => {
                  this.balance = Number(data).toFixed(2);
                });

              await this.web3.getContract()
                .then((contractRes: any) => {
                  if (contractRes) {
                    this.everlens = contractRes;
                    this.getItems();
                  }
                });


              await this.web3.getAuctionContract()
                .then((contractRes: any) => {
                  if (contractRes) {
                    this.auction = contractRes;

                    ///method call to get detail;
                    // this.getItemDetail(this.auction_id)
                    this.getAllAuctions();
                    this.getActiveAuctions();
                    this.getSoldAuctions();
                    this.getAuctionsCount();
                    this.getBidAuctions();
                    this.auctionList = this.ownerAuctions;
                  }
                });

            })
        }
      }, err => {
        if (err) {
        }
      });


  }


  ngOnInit(): void {
    window.scrollTo({
      top: 0,
      left: 0
    })
  }



  getItems() {
    this.everlens.methods.productCount()
      .call()
      .then(async (value) => {
        for (let i = 1; i <= value; i++) {
          const product = await this.everlens.methods.products(i)
            .call()
            .then(async (products) => {
              this.show = false;
              var uri = await this.everlens.methods.tokenURI(i).call().then(async (data) => {

                var uri = data.slice('https://ipfs.io/ipfs/'.length)
                const source = await this.ipfs.cat(uri)
                let contents = ''
                const decoder = new TextDecoder('utf-8')

                for await (const chunk of source) {
                  contents += decoder.decode(chunk, {
                    stream: true
                  })
                }

                contents += decoder.decode()

                var itemMetadata = JSON.parse(contents)
                products['metadata'] = itemMetadata
              });
              if (products.owner == this.accountNumber) {
                if (products.purchased == false) {
                  this.auctionItems.push(products);
                } else {
                  this.boughtItems.push(products);
                }
              }
              this.cd.detectChanges();
            });
        }
      });
  }

  async getAuctionsCount() {
    await this.auction.methods.getCount()
      .call()
      .then(async (value) => {

        for (let i = 0; i < value; i++) {

          await this.auction.methods.auctions(i)
            .call()
            .then(async (auctions) => {
              await this.everlens.methods.products(auctions.productId)
                .call()
                .then(async (products) => {
                  this.show = false;
                  var uri = await this.everlens.methods.tokenURI(auctions.productId).call().then(async (data) => {

                    var uri = data.slice('https://ipfs.io/ipfs/'.length)
                    const source = await this.ipfs.cat(uri)
                    let contents = ''
                    const decoder = new TextDecoder('utf-8')

                    for await (const chunk of source) {
                      contents += decoder.decode(chunk, {
                        stream: true
                      })
                    }

                    contents += decoder.decode()

                    var itemMetadata = JSON.parse(contents)
                    auctions['metadata'] = itemMetadata
                  });
                  auctions['product']=products;
                  var unixtimestamp = auctions.endDate;
                  var months_arr = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

                  // Convert timestamp to milliseconds
                  var date = new Date(unixtimestamp * 1000);

                  // Year
                  var year = date.getFullYear();

                  // Month
                  var month = months_arr[date.getMonth()];

                  // Day
                  var day = date.getDate();

                  // Display date time in MM-dd-yyyy h:m:s format
                  var convdataTime = day + ' ' + month + ' ' + year;
                  auctions['EndDate'] = convdataTime;

                  var ether = await this.web3.getValueFromWei(auctions.endPrice);
                  auctions['EndPrice'] = ether;
                  if (products.owner == this.accountNumber) {
                    if (products.purchased == true) {
                      this.PurchaseItems.push(auctions);
                    }
                  }
                  this.cd.detectChanges();
                });
            });
        }
      });


  }

  getAllAuctions() {
    this.auction.methods.getAuctionsOf(this.accountNumber).call({ from: this.accountNumber }).then(async (data) => {
      for (let i = 0; i < data.length; i++) {
        const auction = this.auction.methods.getAuctionById(data[i])
          .call()
          .then(async (auctions) => {
            var bidCount = await this.auction.methods.getBidsCount(data[i]).call({ from: this.accountNumber })

            var highestBid = await this.auction.methods.getCurrentBid(data[i]).call({ from: this.accountNumber });
            var ether = await this.web3.getValueFromWei(highestBid[0]);
            this.highestBidAmount = ether;
            this.highestBidFrom = highestBid[1];

            var uri = auctions.metadata.slice('https://ipfs.io/ipfs/'.length)
            const source = await this.ipfs.cat(uri)
            let contents = ''
            const decoder = new TextDecoder('utf-8')

            for await (const chunk of source) {
              contents += decoder.decode(chunk, {
                stream: true
              })
            }
            var unixtimestamp = auctions.startDate;
            var months_arr = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

            // Convert timestamp to milliseconds
            var date = new Date(unixtimestamp * 1000);

            // Year
            var year = date.getFullYear();

            // Month
            var month = months_arr[date.getMonth()];

            // Day
            var day = date.getDate();

            // Hours
            var hours = date.getHours();

            // Minutes
            var minutes = "0" + date.getMinutes();

            // Seconds
            var seconds = "0" + date.getSeconds();

            // Display date time in MM-dd-yyyy h:m:s format
            var convdataTime = day + ' ' + month + ' ' + year;

            contents += decoder.decode()

            var decoded_data = JSON.parse(contents)
            this.show = false;
            auctions['meta'] = decoded_data;
            auctions['bidCount'] = bidCount;
            auctions['highestBidAmount'] = ether;
            auctions['highestBidFrom'] = highestBid[1];
            auctions['startTime'] = convdataTime;
            this.ownerAuctions.push(auctions);
            this.cd.detectChanges();
          });
      }
      this.ownerAuctions = this.ownerAuctions.sort((a, b) => (+b.id) - (+a.id));

    })
  }

  getActiveAuctions() {
    this.auction.methods.getAuctionsOf(this.accountNumber).call({ from: this.accountNumber }).then(async (data) => {
      for (let i = 0; i < data.length; i++) {
        const auction = this.auction.methods.getAuctionById(data[i])
          .call()
          .then(async (auctions) => {
            var bidCount = await this.auction.methods.getBidsCount(data[i]).call({ from: this.accountNumber })

            var highestBid = await this.auction.methods.getCurrentBid(data[i]).call({ from: this.accountNumber });
            var ether = await this.web3.getValueFromWei(highestBid[0]);
            this.highestBidAmount = ether;
            this.highestBidFrom = highestBid[1];
            var unixtimestamp = auctions.startDate;
            var months_arr = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

            // Convert timestamp to milliseconds
            var date = new Date(unixtimestamp * 1000);

            // Year
            var year = date.getFullYear();

            // Month
            var month = months_arr[date.getMonth()];

            // Day
            var day = date.getDate();

            // Hours
            var hours = date.getHours();

            // Minutes
            var minutes = "0" + date.getMinutes();

            // Seconds
            var seconds = "0" + date.getSeconds();

            // Display date time in MM-dd-yyyy h:m:s format
            var convdataTime = day + ' ' + month + ' ' + year;


            var uri = auctions.metadata.slice('https://ipfs.io/ipfs/'.length)
            const source = await this.ipfs.cat(uri)
            let contents = ''
            const decoder = new TextDecoder('utf-8')

            for await (const chunk of source) {
              contents += decoder.decode(chunk, {
                stream: true
              })
            }

            contents += decoder.decode()
            var decoded_data = JSON.parse(contents)
            this.show = false;
            auctions['meta'] = decoded_data;
            auctions['bidCount'] = bidCount;
            auctions['highestBidAmount'] = ether;
            auctions['highestBidFrom'] = highestBid[1];
            auctions['startTime'] = convdataTime;
            if (auctions.active == true) {
              this.activeAuctions.push(auctions);
            }
            this.cd.detectChanges();
          });
      }
      debugger
      this.activeAuctions = this.activeAuctions.sort((a, b) => (+b.id) - (+a.id));

    })
  }

  getSoldAuctions() {
    this.auction.methods.getAuctionsOf(this.accountNumber).call({ from: this.accountNumber }).then(async (data) => {
      for (let i = 0; i < data.length; i++) {
        const auction = this.auction.methods.getAuctionById(data[i])
          .call()
          .then(async (auctions) => {
            var bidCount = await this.auction.methods.getBidsCount(data[i]).call({ from: this.accountNumber })

            var highestBid = await this.auction.methods.getCurrentBid(data[i]).call({ from: this.accountNumber });
            var ether = await this.web3.getValueFromWei(highestBid[0]);
            this.highestBidAmount = ether;
            this.highestBidFrom = highestBid[1];

            var uri = auctions.metadata.slice('https://ipfs.io/ipfs/'.length)
            const source = await this.ipfs.cat(uri)
            let contents = ''
            const decoder = new TextDecoder('utf-8')

            for await (const chunk of source) {
              contents += decoder.decode(chunk, {
                stream: true
              })
            }
            var unixtimestamp = auctions.startDate;
            var months_arr = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

            // Convert timestamp to milliseconds
            var date = new Date(unixtimestamp * 1000);

            // Year
            var year = date.getFullYear();

            // Month
            var month = months_arr[date.getMonth()];

            // Day
            var day = date.getDate();

            // Display date time in MM-dd-yyyy h:m:s format
            var convdataTime = day + ' ' + month + ' ' + year;
            contents += decoder.decode()

            var decoded_data = JSON.parse(contents)
            this.show = false;
            auctions['meta'] = decoded_data;
            auctions['bidCount'] = bidCount;
            auctions['highestBidAmount'] = ether;
            auctions['highestBidFrom'] = highestBid[1];
            auctions['startTime'] = convdataTime;
            if (auctions.active == false) {
              this.soldAuctions.push(auctions);
            }
            this.cd.detectChanges();
          });
      }

      this.soldAuctions = this.soldAuctions.sort((a, b) => (+b.id) - (+a.id));

    })
  }



  getBidAuctions() {
    this.auction.methods.getAuctionsOf(this.accountNumber).call({ from: this.accountNumber }).then(async (data) => {
      for (let i = 0; i < data.length; i++) {
        const auction = this.auction.methods.getAuctionById(data[i])
          .call()
          .then(async (auctions) => {
            var bidCount = await this.auction.methods.getBidsCount(data[i]).call({ from: this.accountNumber })
            if(bidCount>0){
            var highestBid = await this.auction.methods.getCurrentBid(data[i]).call({ from: this.accountNumber });
            var ether = await this.web3.getValueFromWei(highestBid[0]);
            this.highestBidAmount = ether;
            this.highestBidFrom = highestBid[1];
            var unixtimestamp = auctions.startDate;
            var months_arr = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

            // Convert timestamp to milliseconds
            var date = new Date(unixtimestamp * 1000);

            // Year
            var year = date.getFullYear();

            // Month
            var month = months_arr[date.getMonth()];

            // Day
            var day = date.getDate();

            // Hours
            var hours = date.getHours();

            // Minutes
            var minutes = "0" + date.getMinutes();

            // Seconds
            var seconds = "0" + date.getSeconds();

            // Display date time in MM-dd-yyyy h:m:s format
            var convdataTime = day + ' ' + month + ' ' + year;


            var uri = auctions.metadata.slice('https://ipfs.io/ipfs/'.length)
            const source = await this.ipfs.cat(uri)
            let contents = ''
            const decoder = new TextDecoder('utf-8')

            for await (const chunk of source) {
              contents += decoder.decode(chunk, {
                stream: true
              })
            }

            contents += decoder.decode()
            var decoded_data = JSON.parse(contents)
            this.show = false;
            auctions['metadata'] = decoded_data;
            auctions['bidCount'] = bidCount;
            auctions['highestBidAmount'] = ether;
            auctions['highestBidFrom'] = highestBid[1];
            auctions['startTime'] = convdataTime;
            if (auctions.active == true) {
              this.bidAuctions.push(auctions);
            }
            this.cd.detectChanges();
          }
          });
        
      }
      this.bidAuctions = this.bidAuctions.sort((a, b) => (+b.id) - (+a.id));

    })
  }


  details(event) {
    event.target.value;
    // if(this.typeValue!=0)  {
    //   this.auctionList = this.ownerAuctions;
    // }

    if (event.target.value == 0) {
      this.auctionList = this.ownerAuctions;
    }
    else if (event.target.value == 1) {
      this.auctionList = this.activeAuctions;
    }
    else if (event.target.value == 2) {
      this.auctionList = this.soldAuctions;
    }
  }
}
