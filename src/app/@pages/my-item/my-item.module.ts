import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { MyItemRoutingModule } from './my-item-routing.module';
import { MyItemComponent } from './my-item.component';


@NgModule({
  declarations: [MyItemComponent],
  imports: [
    CommonModule,
    MyItemRoutingModule,
    TabsModule,
    BsDropdownModule.forRoot()
  ]
})
export class MyItemModule { }
