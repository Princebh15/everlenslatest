import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Web3Service } from 'src/app/service/web3.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-personal-info',
  templateUrl: './personal-info.component.html',
  styleUrls: ['./personal-info.component.scss']
})
export class PersonalInfoComponent implements OnInit {
  userName: any;
  profilePicUrl = '';
  accountNumber: any;
  user_profile = '';
  mediaUrl = 'https://everlens.io:3000/api/image?filename=';
  apiUrl='https://everlens.io:3000/api'
  profileImage:any;
  constructor(private http: HttpClient, private web3: Web3Service,private toastrService:ToastrService) {
    this.userName = JSON.parse(localStorage.getItem('username'));
  }

  async ngOnInit(): Promise<void> {
    window.scrollTo({
      top: 0,
      left: 0
    })
    await this.getUserName();
    this.getProfileImage();
  }
  async getUserName() {
  await  this.web3.checkAndInstantiateWeb3()
      .then(async (checkConn: any) => {

        if (checkConn === 'connected') {
        await  this.web3.loadBlockChainData()
            .then((accountData: any) => {
              this.accountNumber = accountData[0];
            });
        }
      });
  }
  uploadPhotos(event) {
    let file = event.target.files[0];
    let fileName = '';
    if (file) {
      const formData = new FormData();
      formData.append('file', file);
      this.http.post(this.apiUrl +'/image', formData, { responseType: 'json' }).pipe(map((res: any) => {
        return res;
      })).subscribe((data: any) => {
        fileName = data.filename;
        this.user_profile = fileName
        this.saveProfile(fileName);
        this.showProfilePic(fileName);

      });
    }
  }
  showProfilePic(fileName){
    this.http.get(this.apiUrl +'/image?filename=' + fileName, { responseType: 'blob' }).subscribe((res: any) => {
        this.createImageFromBlob(res);
      })
  }
  saveProfile(fileName){
    let payload={
      address:this.accountNumber,
      image:fileName
    }
    this.http.post(this.apiUrl +'/profile',payload).subscribe((res:any)=>{
      this.toastrService.success('Updated Successfully.');
    })
  }

  createImageFromBlob(image: Blob) {
    let reader = new FileReader();
    reader.addEventListener("load", () => {
       this.profileImage = reader.result;
    }, false);
 
    if (image) {
       reader.readAsDataURL(image);
    }
 }
 getProfileImage(){
  this.http.get(this.apiUrl +'/profile?address='+this.accountNumber).subscribe((res:any)=>{
    // this.showProfilePic(res[0].image);
    if (res[0]?.image) {
      this.showProfilePic(res[0].image);
    }else{
      this.profileImage =''
    }
  })
 }
}
