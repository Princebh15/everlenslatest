import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {


    transform(list: any[], filterText: string): any {
      filterText = filterText ? filterText : "";
      if(!filterText){
        return list;
      }else{
       return list.filter((r)=>r.name.toLowerCase().indexOf(filterText.toLowerCase())!=-1 || r.pro_creator.toLowerCase().indexOf(filterText.toLowerCase())!=-1)
      }
    }
  

}
