import { Component, OnInit } from '@angular/core';
import { Web3Service } from 'src/app/service/web3.service';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.scss']
})
export class WalletComponent implements OnInit {

  accountNumber: any;
  balance: any;
  constructor(private web3: Web3Service) { }

  ngOnInit(): void {
    window.scrollTo({
      top: 0,
      left: 0
    })
    this.web3.checkAndInstantiateWeb3()
      .then((checkConn: any) => {
        if (checkConn === 'connected') {
          this.web3.loadBlockChainData()
            .then((accountData: any) => {
              this.accountNumber = accountData[0];
              this.web3.getEtherBalance(this.accountNumber)
                .then((data: any) => {
                  this.balance = Number(data).toFixed(2);
                });
            })
        }
      })
  }
}
