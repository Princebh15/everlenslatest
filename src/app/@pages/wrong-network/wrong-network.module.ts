import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WrongNetworkRoutingModule } from './wrong-network-routing.module';
import { WrongNetworkComponent } from './wrong-network.component';
import { ModalModule } from 'ngx-bootstrap/modal';


@NgModule({
  declarations: [
    WrongNetworkComponent
  ],
  imports: [
    CommonModule,
    WrongNetworkRoutingModule,
    ModalModule.forRoot()
  ]
})
export class WrongNetworkModule { }
