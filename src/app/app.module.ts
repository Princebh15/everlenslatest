import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InstaImagesComponent } from './@pages/modals/insta-images/insta-images.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { HttpClientModule } from '@angular/common/http';
import { NoMetaMaskComponent } from './@pages/no-meta-mask/no-meta-mask.component';
import { TermsConditionComponent } from './terms-condition/terms-condition.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { ToastrModule } from 'ngx-toastr';
import { AboutUsComponent } from './@pages/about-us/about-us.component';

@NgModule({
  declarations: [
    AppComponent,
    InstaImagesComponent,
    NoMetaMaskComponent,
    TermsConditionComponent,
    PrivacyPolicyComponent,
    AboutUsComponent,    
  ],
  imports: [
    ToastrModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgbModule,
    ModalModule.forRoot(),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
