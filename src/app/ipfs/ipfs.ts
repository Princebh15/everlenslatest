
import { InjectionToken } from '@angular/core';
import IpfsHttpClient from 'ipfs-http-client';

export const IPFS = new InjectionToken(
  'The IPFS instance',
  {
    providedIn: 'root',
    factory: () => {
      try {
        return new IpfsHttpClient({ host: 'ipfs.infura.io', port: '5001', 
        // apiPath: '/ipfs/api/v0',
         protocol: 'https' });
      } catch (err) {
        throw new Error('Unable to access IPFS node daemon on Infura network');
      }
    }
  },
);

export function initIPFS(node) {
  return function () {
    return new Promise((resolve, reject) => {
      // node.on('error', () => reject());
      // node.on('ready', () => resolve());
    });
  };
}